var Voting = artifacts.require("./Voting.sol");
const { GAS_LIMIT } = require('../config.js')


module.exports = function(deployer) {
  deployer.deploy(Voting, ['Armen', 'Mycah', 'RainbowKitty'], { gas: GAS_LIMIT });
  /**
   * this function originally had a third argument - { gas: 29000 } but was deleted
   * because it wouldn't work on the testnet
   */
};
/**
 * The deployer expects the first argument to be the name of the contract followed by constructor arguments.
 * In this case, there is only one argument which is an array of candidates. The third argument is a hash
 * where the gas required to deploy the code is specified. The gas amount varies depending on the size of the contract.
*/
