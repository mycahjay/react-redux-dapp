// Allows us to use ES6 in our migrations and tests.
require('babel-register')
const { GETH_BLOCK_ADDRESS, GAS_LIMIT } = require('./.env')

module.exports = {
  networks: {
    development: {
      host: '127.0.0.1',
      port: 7545, /* FIXME: should this be 8545? */
      from: GETH_BLOCK_ADDRESS, /* FIXME: do I need this? */
      network_id: '*', /* Matches any network id */
      gas: GAS_LIMIT
    }
  }
}
