/**
* dev webpack config
*/
const merge = require('webpack-merge')
const baseConfig = require('./base.config.js')
const CleanWebpackPlugin = require('clean-webpack-plugin')

const pathsToClean = [ 'dist' ]
const cleanOptions = {
  /* absolute path to webpack root folder (default: root of package) */
  root: process.cwd(),
  /* write logs to console */
  verbose: true,
  /* if set to true, will not remove files (default: false) */
  dry: false,
  /* remove files on recompile (default: false) */
  watch: false,
  /* set files to exclude */
  exclude: [ 'assets', 'index.html' ],
  /* allow plugin to clean folders outside of webpack root (default: false) */
  allowExternal: false
}

module.exports = merge(baseConfig, {
  devtool: 'cheap-module-source-map',
  module: {
    rules: [{
      test: /\.css$/,
      exclude: /node_modules/,
      use: [
        'style-loader',
        'css-loader?modules&camelCase=dashes'
      ]
    }]
  },
  plugins: [ new CleanWebpackPlugin(pathsToClean, cleanOptions) ]
})
