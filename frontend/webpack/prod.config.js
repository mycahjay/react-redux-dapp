/**
* production webpack config
*/
const webpack = require('webpack')
const merge = require('webpack-merge')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const baseConfig = require('./base.config.js')

const extractCSS = new ExtractTextPlugin('main.style.css')

module.exports = merge(baseConfig, {
  devtool: 'source-map',
  module: {
    rules: [{
      test: /\.css$/,
      loader: extractCSS.extract({
        fallback: 'style-loader',
        use: [
          { loader: 'css-loader?modules&camelCase=dashes' },
          { loader: 'postcss-loader' }
        ]
      })
    }]
  },
  plugins: [
    extractCSS,
    new webpack.optimize.UglifyJsPlugin({
      compress: { screw_ie8: true, warnings: false },
      sourceMap: true
    })
  ]
})
