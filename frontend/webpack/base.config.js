/**
 * base webpack config
 */
const webpack = require('webpack')
const path = require('path')
const Dotenv = require('dotenv-webpack')
const autoprefixer = require('autoprefixer')
const DashboardPlugin = require('webpack-dashboard/plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const SRC_DIR = path.join(__dirname, '../src')
const DIST_DIR = path.join(__dirname, '../dist')

const dotenv = () => new Dotenv({ systemvars: process.env.CI })

module.exports = {
  entry: [
    'babel-polyfill',
    'react-hot-loader/patch',
    path.join(SRC_DIR, 'index.js')
  ],
  resolve: { extensions: ['.js', '.json', '.css'] },
  output: {
    path: DIST_DIR,
    filename: 'bundle.js',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        loader: 'babel-loader?cacheDirectory=true'
      },
      { test: /\.(png|jpg|jpeg|gif|ico)$/, loader: 'url-loader' }
    ]
  },
  plugins: [
    dotenv(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new DashboardPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
  ],
  devServer: {
    contentBase: DIST_DIR,
    compress: true,
    hot: true,
    noInfo: true,
    overlay: { warnings: true, errors: true }
  }
}
