import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import { Provider } from 'react-redux'
import { HashRouter, Switch, Route } from 'react-router-dom'
import { Redirect } from 'react-router'
import injectTapEventPlugin from 'react-tap-event-plugin'
import configureStore from './store'

/* components */
import { Page404 } from './pages'
import { ErrorBoundary } from './containers'
import App from './App'

/* * style * */
import './main.style.css'

injectTapEventPlugin()

const init = async () => {
  const store = await configureStore()

  const handleRedirect = () => {
    const { authenticated } = store.getState().auth
    return <Redirect to={authenticated ? '/home' : '/'} />
  }

  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <ErrorBoundary component='index.js'>
          <HashRouter>
            <Switch>
              <Route exact path='/404' name='Page 404' component={Page404} />
              <Route exact path='/' component={handleRedirect} />
              <Route component={App} />
            </Switch>
          </HashRouter>
        </ErrorBoundary>
      </Provider>
    </AppContainer>,
    document.getElementById('root')
  )

  if (module.hot) {
    module.hot.accept('./App.js', () => {
      ReactDOM.render(App)
    })
  }
}

init()
