import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import Lock from '../../lib/auth'

/* * actions * */
import { logOut } from '../../store/actions'

/* * components * */
import { Button } from '../'
import { ErrorBoundary } from '../../containers'

/* * style * */
import style from './style.css'

class Header extends Component {
  handleNavigation = (path) => this.props.history.push(path)

  handleLogIn = () => Lock.show()

  handleLogOut = () => this.props.logOut()

  render () {
    return (
      <ErrorBoundary component='Header'>
        <header className={style.main}>
          <div className={style.brand}>
            <img src='assets/unicorn-emoji.png' />
            <div className={style.title}>react-redux-dapp</div>
          </div>
          <div className={style.navContainer}>
            <Button type='nav-main' label='home' onButtonClick={() => this.handleNavigation('/')} />
            <Button type='nav-main' label='log in' onButtonClick={this.handleLogIn} />
            <Button type='nav-main' label='log out' onButtonClick={this.handleLogOut} />
          </div>
        </header>
      </ErrorBoundary>
    )
  }
}

const mapStateToProps = ({ auth }) => ({ auth })

export default withRouter(connect(mapStateToProps, { logOut })(Header))
