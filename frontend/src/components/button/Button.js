import React from 'react'
import style from './style'

export const Button = (props) => (
  <button
    className={style[props.type]}
    onClick={props.onButtonClick}
  >
    {props.label}
  </button>
)

export default Button
