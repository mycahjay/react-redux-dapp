import auth0Config from './auth0'
import blockConfig from './block'

const env = process.env.CI ? 'prod' : process.env.NODE_ENV

module.exports = {
  env,
  ...auth0Config,
  ...blockConfig
}
