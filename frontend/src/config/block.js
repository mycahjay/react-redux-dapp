import { default as contract } from 'truffle-contract'

import votingArtifacts from '../../../block/build/contracts/Voting.json'
/*
 * When you compile and deploy your Voting contract,
 * truffle stores the abi and deployed address in a json
 * file in the build directory. We will use this information
 * to setup a Voting abstraction. We will use this abstraction
 * later to create an instance of the Voting contract.
 * Compare this against the index.js from our previous tutorial to see the difference
 * https://gist.github.com/maheshmurthy/f6e96d6b3fff4cd4fa7f892de8a1a1b4#file-index-js
 */

const Voting = contract(votingArtifacts)

module.exports = {
  Voting,
  BLOCK_URL: 'http://localhost:8545'
  // BLOCK_ADDRESS: process.env.BLOCK_ADDRESS
}
