import React, { Component } from 'react'
import { connect } from 'react-redux'
import style from './style'

class ErrorBoundary extends Component {
  state = { error: null, errorInfo: null }

  componentDidCatch = (error, errorInfo) => this.setState({ error, errorInfo })

  render () {
    if (this.state.errorInfo) {
      return (
        <div className={style.main}>
          <h2>{`Error in ${this.props.component} 😱`}</h2>
          <details style={{ whiteSpace: 'pre-wrap' }}>
            {this.state.error && this.state.error.toString()}
            <br />
            {this.state.errorInfo.componentStack}
          </details>
        </div>
      )
    }
    return this.props.children
  }
}

export default connect(null, null)(ErrorBoundary)
