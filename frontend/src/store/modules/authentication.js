const LOG_IN_REQUEST = 'LOG_IN_REQUEST'
const LOG_IN_SUCCESS = 'LOG_IN_SUCCESS'
const LOG_IN_FAILURE = 'LOG_IN_FAILURE'
const LOG_OUT = 'LOG_OUT'

/**
 * Actions
 */
const requestLogIn = () => ({
  type: LOG_IN_REQUEST
})

const logInSuccess = ({ user }) => ({
  type: LOG_IN_SUCCESS,
  payload: { user }
})

const logInFailure = ({ message }) => ({
  type: LOG_IN_FAILURE,
  payload: { message }
})

const logOut = () => ({
  type: LOG_OUT
})

/**
 * Reducer
 */
const authReducer = (state = {}, action) => {
  switch (action.type) {
    case LOG_IN_REQUEST: {
      return {
        ...state,
        authenticating: true,
        authenticated: false
      }
    }
    case LOG_IN_SUCCESS: {
      const { user } = action.payload
      return {
        ...state,
        authenticating: false,
        authenticated: true,
        user
      }
    }
    case LOG_IN_FAILURE: {
      const { message } = action.payload
      return {
        ...state,
        authenticating: false,
        authenticated: false,
        errorMessage: message
      }
    }
    case LOG_OUT: {
      return {
        ...state,
        /* FIXME: change authenticated to false */
        authenticating: false,
        authenticated: true
      }
    }
    default: return state
  }
}

module.exports = {
  actions: { requestLogIn, logInSuccess, logInFailure, logOut },
  reducer: authReducer
}
