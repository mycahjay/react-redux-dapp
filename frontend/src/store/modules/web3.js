const WEB3_SET = 'WEB3_SET'

/**
 * Actions
 */
const setWeb3 = ({ web3 }) => ({
  type: WEB3_SET
})

/**
 * Reducer
 */
const web3Reducer = (state = {}, action) => {
  switch (action.type) {
    case WEB3_SET: {
      const { web3 } = action.payload
      return {
        ...state,
        web3
      }
    }
    default: return state
  }
}

module.exports = {
  actions: { setWeb3 },
  reducer: web3Reducer
}
