const { requestLogIn, logInSuccess, logInFailure, logOut } = require('./modules/authentication').actions
const { setWeb3 } = require('./modules/web3').actions

module.exports = {
  requestLogIn,
  logInSuccess,
  logInFailure,
  logOut,
  setWeb3
}
