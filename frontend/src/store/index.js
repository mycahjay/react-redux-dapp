import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { persistStore, autoRehydrate } from 'redux-persist'
import thunk from 'redux-thunk'
import { reducer, initialState } from './reducers'

const middlewares = [ thunk ]
const enhancers = [ applyMiddleware(...middlewares), autoRehydrate() ]

export const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(...enhancers)
)

const configureStore = () => {
  return new Promise((resolve, reject) => {
    try {
      persistStore(store, {}, () => resolve(store))
    } catch (error) {
      console.log(error)
      reject(error)
    }
  })
}

export default configureStore
