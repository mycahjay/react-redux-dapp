import { combineReducers } from 'redux'

/* * Reducers * */
const LOG_OUT = 'LOG_OUT'

const appReducer = combineReducers({
  auth: require('./modules/authentication').reducer,
  web3: require('./modules/web3').reducer
})

export const initialState = {
  auth: {
    authenticated: true,
    authenticating: false,
    user: false
  },
  web3: false
}

export const reducer = (state, action) => (
  appReducer(action.type === LOG_OUT ? initialState : state, action)
)
