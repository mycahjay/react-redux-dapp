import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'

/* * actions * */

/* * components * */
import { ErrorBoundary } from '../../containers'

/* * style * */

export class Page404 extends Component {
  render () {
    return (
      <ErrorBoundary component='Page 404'>
        <div>
          Page404
        </div>
      </ErrorBoundary>
    )
  }
}

export default withRouter(connect(null, null)(Page404))
