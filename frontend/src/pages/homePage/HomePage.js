import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'

import { default as Web3 } from 'web3'
import { BLOCK_URL } from '../../config/block'
import { Voting } from '../../lib/ethereum'

/* * actions * */

/* * components * */
import { Button } from '../../components'
import { ErrorBoundary } from '../../containers'

/* * style * */
import style from './style.css'

const candidates = ['Armen', 'Mycah', 'RainbowKitty']

export class HomePage extends Component {
  state = {
    votes_Armen: 0,
    votes_Mycah: 0,
    votes_RainbowKitty: 0
  }

  web3 = false

  componentDidMount = () => {
    if (this.web3 === undefined) {
      console.warn('Web3 is undefined (detected from external source like Metamask). Make sure to error handle this later.')
      this.web3 = new Web3(this.web3.currentProvider)
    } else {
      console.warn(`No Web3 detected! Falling back to ${BLOCK_URL}. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask`)
      this.web3 = new Web3(new Web3.providers.HttpProvider(BLOCK_URL))
    }
    Voting.setProvider(this.web3.currentProvider)
    candidates.forEach(candidate => {
      Voting.deployed().then((contractInstance) => {
        const updatedVote = contractInstance.totalVotesFor.call(candidate)
        this.setState({
          [`votes_${candidate}`]: updatedVote
        })
      })
    })
  }

  // handleVote = (candidate) => {
  //   contractInstance.voteForCandidate(candidate, {from: web3.eth.accounts[0]})
  //   const updatedVote = contractInstance.totalVotesFor.call(candidate)
  //   this.setState({
  //     [`votes_${candidate}`]: updatedVote
  //   })
  // }

  handleVote = (candidate) => {
    try {
      Voting.deployed().then((contractInstance) => {
        contractInstance.voteForCandidate(candidate, { gas: 1400, from: Web3.eth.accounts[0] })
        const updatedVote = contractInstance.totalVotesFor.call(candidate)
        this.setState({
          [`votes_${candidate}`]: updatedVote
        })
      })
    } catch (error) {
      console.log(error)
    }
  }

  render () {
    return (
      <ErrorBoundary component='Home Page'>
        <div className={style.main}>
          <div className={style.candidatesContainer}>
            <div className={style.candidate}>
              <div>Armen</div>
              <div>{`Votes: ${this.state.votes_Armen}`}</div>
              <Button
                label='Vote!'
                type='vote'
                onClick={() => this.handleVote('Armen')}
              />
            </div>
            <div className={style.candidate}>
              <div>Mycah</div>
              <div>{`Votes: ${this.state.votes_Mycah}`}</div>
              <Button
                label='Vote!'
                type='vote'
                onClick={() => this.handleVote('Mycah')}
              />
            </div>
            <div className={style.candidate}>
              <div>Rainbow Kitty</div>
              <div>{`Votes: ${this.state.votes_RainbowKitty}`}</div>
              <Button
                label='Vote!'
                type='vote'
                onClick={() => this.handleVote('RainbowKitty')}
              />
            </div>
          </div>
        </div>
      </ErrorBoundary>
    )
  }
}

export default withRouter(connect(null, null)(HomePage))
