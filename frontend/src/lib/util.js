export const getUnixTime = () => Math.floor(Date.now() / 1000)

export const getTime = () => {
  const date = new Date()
  const secs = date.getSeconds()
  const mins = date.getMinutes()
  const hour = date.getHours()
  return `${hour}:${mins}:${secs}`
}

const months = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ]

export const getDateFromUnixTimestamp = (timestamp) => {
  const date = new Date(timestamp * 1000)
  const month = months[date.getMonth()]
  const day = date.getDate()
  return `${month} ${day}`
}

export const getFromLS = (items) => {
  const results = {}
  for (const [item, key] of Object.entries(items)) {
    results[item] = localStorage.getItem(key) || false
  }
  return results
}

export const saveToLS = (items) => {
  for (const [key, value] of Object.entries(items)) {
    localStorage.setItem(key, value)
  }
}

export const removeFromLS = (items) => (
  items.forEach(item => localStorage.removeItem(item))
)
