import { default as Web3 } from 'web3'
import { Voting, BLOCK_URL } from '../config/block'
import { setWeb3 } from '../store/actions'
import { store } from '../store'

export const voteForCandidate = (candidate) => {
  return new Promise(async (resolve, reject) => {
    try {
      Voting.deployed().then(contractInstance => {
        contractInstance.voteForCandidate(candidate, { gas: 1400, from: Web3.eth.accounts[0] })
        const updatedVote = contractInstance.totalVotesFor.call(candidate)
        return resolve({ updatedVote, success: true })
      })
    } catch (error) {
      console.log(error)
      return resolve({ success: false })
    }
  })
}

export const getAllVoteCounts = (candidates, currentWeb3Provider) => {
  return new Promise(async (resolve, reject) => {
    try {
      console.log('getting web3')
      const web3 = store.getState().web3 || new Web3(new Web3.providers.HttpProvider(BLOCK_URL))
      console.log(web3)
      store.dispatch(setWeb3({ web3 }))
      Voting.setProvider(web3.currentProvider)
      const updatedVotes = candidates.reduce((votes, candidate) => {
        Voting.deployed().then(contractInstance => {
          const updatedVote = contractInstance.totalVotesFor.call(candidate)
          votes[candidate] = updatedVote
        })
        return votes
      }, {})
      console.log('updated votes')
      console.log(updatedVotes)
      return resolve(updatedVotes)
    } catch (error) {
      console.log(error)
      return resolve(false)
    }
  })
}
