import Auth0Lock from 'auth0-lock'
import store from '../store'
import { AUTH0_CLIENT_ID, AUTH0_DOMAIN } from '../config'
import { logInFailure, logInSuccess } from '../store/actions'

const options = {
  auth: { redirect: false, responseType: 'token id_token' },
  // params: { scope: 'openid email'},
  allowedConnections: ['Username-Password-Authentication', 'google-oauth2', 'facebook'],
  rememberLastLogin: false,
  socialButtonStyle: 'big',
  languageDictionary: {
    loginSubmitLabel: 'get nomming',
    signUpSubmitLabel: 'get nomming',
    submitLabel: 'get nomming',
    title: 'nom'
  },
  language: 'en',
  theme: {
    logo: 'http://cdn.shopify.com/s/files/1/1061/1924/products/Unicorn_Emoji_grande.png?v=1480481038',
    primaryColor: '#273547'
  }
}

const Lock = new Auth0Lock(AUTH0_CLIENT_ID, AUTH0_DOMAIN, options)

Lock.on('authenticated', (authResult) => {
  Lock.getProfile(authResult.idToken, (error, profile) => (
    store.dispatch(error
      ? store.dispatch(logInFailure({ message: error.message }))
      : store.dispatch(logInSuccess({ user: { ...profile, ...authResult } }))
    )
  ))
  Lock.hide()
})

Lock.on('authorization_error', (error) => {
  store.dispatch(logInFailure({ message: error.message }))
  Lock.show({
    flashMessage: { type: 'error', text: error.error_description }
  })
})

export default Lock
