import React, {Component} from 'react'
import { connect } from 'react-redux'
import { Switch, Route } from 'react-router-dom'
import { Redirect, withRouter } from 'react-router'
import { store } from './store'
import cx from 'classnames'

/* * components * */
import { Header, LoadingSpinner } from './components'
import { ErrorBoundary } from './containers'
import { HomePage } from './pages'

const PrivateRoute = ({ component: Component }) => {
  const { authenticated } = store.getState().auth
  return <Route render={() => authenticated ? <Component /> : <Redirect to='/welcome' />} />
}

export class App extends Component {
  render () {
    return (
      <ErrorBoundary component='App'>
        <Header />
        <div className='app-body'>
          <Switch>
            <PrivateRoute path='/home' component={HomePage} />
            <Redirect to='/404' />
          </Switch>
        </div>
      </ErrorBoundary>
    )
  }
}

const mapStateToProps = ({ auth }) => ({ auth })

export default withRouter(connect(mapStateToProps, null)(App))
