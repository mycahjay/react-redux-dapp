# AWS Environment Management Platform

[![CircleCI](https://circleci.com/gh/DADC-NMS/em-interface/tree/master.svg?style=shield&circle-token=a93beb66e8ebf03c23ee8eb9d5d66a2f1f909b1f)](https://circleci.com/gh/DADC-NMS/em-interface/tree/master)


## About

Interface providing an overview of NMS' AWS infrastructure and real-time status of currently running applications and respective services.

## Stack

- `React 16`: A JavaScript Library | [Docs](https://reactjs.org/) |
 [v16 Release Blog](https://reactjs.org/blog/2017/09/26/react-v16.0.html) |

- `Redux`: A Predictable State Container of Awesomeness | [Docs](http://redux.js.org/) |

- `Jest`: Delightful JavaScript Testing | [Docs](https://facebook.github.io/jest/) |

- `Enzyme`: Testing Utility for React | [Docs](http://airbnb.io/enzyme/) |

- `Standard JS`: One JavaScript Style to Rule Them All | [Docs](https://standardjs.com/index.html) |

- `Webpack`: Bundle Your Assets | [Docs](https://webpack.js.org/) |

## Useful Dev Tools

### [Git Secrets](https://github.com/awslabs/git-secrets)

Prevents you from committing secrets and credentials into git repositories. Made by [AWS Labs](https://github.com/awslabs).

### [Redux DevTools](https://github.com/gaearon/redux-devtools)

DevTools for Redux with hot reloading and action replay. Use with project-integrated customizable UI or [browser extension](http://extension.remotedev.io/) (recommended).

## Authentication

### [AWS Cognito](https://aws.amazon.com/documentation/cognito/)

Grants temporary AWS credentials to [authenticated](https://www.pingidentity.com/fr/platform/single-sign-on/software-sso.html) users.


#### AWS Identity Pool ####

- dev: `supplychain_dev_pingfed`
- prod: `supplychain_pingfed`


## Integrations

### [SpotInst](https://spotinst.com)

Service used to manage Amazon Spot Instances. Automatically bids for instances and scales instances as needed. 

- [API](http://docs.spotinst.com/api/#)

- [Console](https://console.spotinst.com/)